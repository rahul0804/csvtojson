import json
import csv
import os
c=os.getcwd()
index=[]
first_rowtype=[]
m=0
def file_path():
    print("Enter the file name")
    path=input()
    try:
        with open(path,'r') as r:
            csv_reader = csv.reader(r)
            for i in csv_reader:
                for j in i:
                    index.append(j)
                break
        print('File Found')   
        return path 
    except:
        print("file not found") 
        print("IF you want to type the file name again(y/n)")
        s=input()
        if s.startswith('y'):
            file_path()
        else:
            print('Thank you')       
    return(path)            

def match(dtype):
    print('\n')
    print('Enter data in Correct Data Type\n')
    n=input()
    try:
        n=float(n)
    except:
        n=n
    if type(n)==dtype:
        return n
    else:
        match(dtype)            
    
def first_row(path):
    with open(path,'r') as r:
        csv_reader = csv.reader(r)
        count=0
        for i in csv_reader:
            if count==1:
                for j in i:
                    try:
                        j=float(j)
                        first_rowtype.append(type(j))
                    except:
                        first_rowtype.append(type(j))    
            count=count+1        

def addition(path,row,m):
    if m!=1:
        with open(path,'a') as a:
            wr=csv.writer(a,quoting = csv.QUOTE_NONNUMERIC)
            wr.writerow(row)
            a.close()
    print("\nDo you want to Add Row(y/n)")
    option=input()
    if option.startswith('y'):
        getting_input(path)    
    else:
        print("ok,byee")
        print('\n') 
def getting_input(path):
    lst=[]
    first_row(path)
    if len(first_rowtype)>0:
        for i in range(len(index)):
            print("Enter value for ",index[i])
            input_row=input()
            try:
                input_row=float(input_row)
            except:
                input_row=input_row
            if type(input_row)==first_rowtype[i]:
                lst.append(input_row)
            else:
                print('You entered wrong datatype')
                lst.append(match(first_rowtype[i]))          
    else:
        for i in range(len(index)):
            print("Enter ",index[i])
            input_row=input()
            try:
                input_row=float(input_row)
            except:
                input_row=input_row    
            lst.append(input_row)
    if len(first_rowtype)==0:
        first_row(path)    
    addition(path,lst,0)    

def show(path):
    with open(path,'r') as r:
        csv_reader = csv.reader(r)
        print("Current CSV File")
        j=[]
        for i in csv_reader:
            j.append(i[:])
        return j
input_csv=input("Hello,Do you have CSV file-(y/n)")
input_csv=input_csv.lower()

if input_csv.startswith('y'):
    path=file_path()
    addition(path,[],0)  
    q=path.split('/') 
    s=q[len(q)-1].split('.')
    na=s
else:
    v='y'
    na=input("\nEnter File Name   ")
    cd=na+'.csv'
    path=c+'/'+cd
    print("Your File Path\n",path)
    print('\n')
    n=int(input('Enter Number of Columns   '))
    t=[]
    print('Enter the column name\n')
    for i in range(n):
        check=1
        print(i+1,'column name')
        k=input()
        index.append(k)
        t.append(k)
    addition(path,t,0) 


jsonFilePath=r'LocalJSON.json'              
import json
 
def make_json(csvFilePath, jsonFilePath):
    data = {}
    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
        count=0
        for rows in csvReader:
            key = count
            count=count+1
            data[key] = rows    
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
        jsonf.write(json.dumps(data, indent=4))

    with open(jsonFilePath,'r')as p:
        data=json.load(p)
        print("Your JSON file path is\n ",jsonFilePath)
        print(data)

make_json(path, jsonFilePath)
 